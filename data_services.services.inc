<?php


function data_services__service__table_access($op, $table) {
  $table_in_path = ds_get_requested_table_name_from_path($op);
  if (!db_table_exists($table_in_path) || $table != $table_in_path) {
    return FALSE;
  }
  
  if ($op == 'index') {
  }
  else if ($op == 'retrieve') {
  }
  return TRUE;
}

function data_services__service__table_retrieve($primary_keys) {
  $table_name = ds_get_requested_table_name_from_path('retrieve');
  $primary_keys = explode(',', $primary_keys);
  $where = array();
  foreach ($primary_keys as $key) {
    list($field, $value) = explode(':', $key);
    if (!$value) { // just the primary key's value specified
      $where = $field;
      break;
    }
    else {
      $where[$field] = $value;
    }
  }
  if (!is_array($where)) { // only primary key specified
    if (count(ds_primary_key($table_name)) > 1) {
      return FALSE; // error: more than one primary key in this table
    }
    $pk_field = ds_primary_key($table_name);
    if (is_array($pk_field)) {
      $pk_field = $pk_field[0];
    }
    $where = array($pk_field => $where);
  }
  
  $q = "SELECT * FROM {{$table_name}} WHERE ";
  $clauses = array();
  foreach ($where as $field => $value) {
    $placeholder = ds_placeholder($table_name, $field);
    $clauses[] = "$field = $placeholder";
  }
  $q .= implode(' AND ', $clauses);
  $res = db_query($q, array_values($where));
  return db_fetch_object($res);
}

function data_services__service__table_index() {
  $table_name = ds_get_requested_table_name_from_path('index');
  $q = "SELECT * FROM {{$table_name}}";
  $res = db_query($q);
  $rows = array();
  while ($row = db_fetch_object($res)) {
    $rows[] = $row;
  }
  return $rows;
}

/**
 * Implementation of hook_services_resources().
 */
function data_services_services_resources() {
  $resources = array();
  $tables = module_invoke_all('data_services_tables');
  foreach ((array)$tables as $table_name) {
    $resources[$table_name] = array(
      'retrieve' => array(
        'help' => '',
        'file' => array('type' => 'inc', 'module' => 'data_services', 'name' => 'data_services.services'),
        'callback' => 'data_services__service__table_retrieve',
        'access callback' => 'data_services__service__table_access',
        'access arguments' => array('retrieve', $table_name),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'primary_keys',
            'type' => 'string',
            'description' => 'Comma separated list of primary keys of the row to fetch ("nid:1,uid:43.json" or "name:foo,bar,baz.json" or if only one primary key is specified, simply "2493.json")',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
        ),
      ),
     'index' => array(
       'help' => 'Retrieves a listing of all available rows',
       'file' => array('type' => 'inc', 'module' => 'data_services', 'name' => 'data_services.services'),
       'callback' => 'data_services__service__table_index',
       'access callback' => 'data_services__service__table_access',
       'access arguments' => array('index', $table_name),
       'access arguments append' => FALSE,
       'args' => array(),
     ),
     
     /*'actions' => array(
        'download' => array(
          'help' => 'Retrieve an export package based on the passed array of settings',
          'file' => array('type' => 'inc', 'module' => 'data_services', 'name' => 'data_services.services'),
          'callback' => '_data_services_service__package__download',
          'access callback' => '_data_services_service_access',
          'access arguments' => array('package', 'download'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'settings',
              'type' => 'array',
              'description' => 'An array of plugin names keyed to arrays of object IDs keyed to export settings for each object',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ), // end download
      ),*/ // end actions
    );
  }
  return $resources;
}



// @@RECENT-CHANGE
function data_services_default_services_endpoint() {
  /*$endpoints = array();
  $endpoint = new stdClass;
  $endpoint->api_version = 3;
  $endpoint->debug = 0;
  $endpoint->status = 1;
  $endpoint->name = 'data_services';
  $endpoint->title = 'Import/Export module Export Server';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'export-server';
  $endpoint->authentication = array();
  $endpoint->resources = array(
    'data_services' => array(
      'alias' => '',
      'operations' => array(
        'retrieve' => array(
          'enabled' => 1,
        ),
        'index' => array(
          'enabled' => 1,
        ),
      ),
      'targeted actions' => array(
        'download' => array(
          'enabled' => 1,
        ),
      ),
      'actions' => array(
        'download' => array(
          'enabled' => 1,
        ),
      ),
    ),
  );
  $endpoints[] = $endpoint;
  return $endpoints;*/
}




